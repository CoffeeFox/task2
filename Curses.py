import time
import curses

screen = curses.initscr()
curses.noecho()
curses.start_color()


i = []
lines = ''
# Кадры анимации
f = open('earth.md', 'r')
while True:
        line = f.readline()
        if len(line) < 7:
            if lines != '':
                i.append(lines)
            lines = ''
        else:
            lines = lines + line
        if len(line) == 0:
            break
f.close()


# Цвет кадров
curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
# Бесконечное воспроизведение кадров анимации по очереди
while True:
    for f in range(len(i)):
        # или os.system('clear') для Unix
        try:
            screen.addstr(0, 0, i[f], curses.color_pair(1))
        except curses.error:
            pass
        screen.refresh()
        time.sleep(0.2)
